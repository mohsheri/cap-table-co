import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  baseUrl = 'https://testdomain.com';

  constructor(private http: HttpClient) { }

  submitRowDetails(rowDetails: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/api/submit`, rowDetails);
  }
}
