import { TestBed } from '@angular/core/testing';

import { DataService } from './data.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('DataService', () => {

  let service: DataService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [DataService]
  }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should return inserted records', () => {
    const mockData = [{
      name: 'test-name-1',
      id: 1
    },
    {
      name: 'test-name-2',
      id: 2
    }];

    service = TestBed.get(DataService);
    httpMock = TestBed.get(HttpTestingController);

    service.getData().subscribe((records) => {
      expect(records.length).toBe(2);
    });

    const request = httpMock.expectOne(`${service.jsonURL}`);

    expect(request.request.method).toBe('GET');

    request.flush(mockData);

  });
});
