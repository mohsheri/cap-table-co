import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';

import { ApiService } from './api.service';
import { HttpClient } from '@angular/common/http';

describe('ApiService', () => {

  let service: ApiService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule],
    providers: [ApiService]
  }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should return inserted records', () => {
    const mockData = {
      name: 'test-name-1',
      id: 1
    };

    service = TestBed.get(ApiService);
    httpMock = TestBed.get(HttpTestingController);

    service.submitRowDetails(mockData).subscribe((records) => {
      expect(records).toBe(mockData);
    });

    const request = httpMock.expectOne(`${service.baseUrl}/api/submit`);

    expect(request.request.method).toBe('POST');

    request.flush(mockData);

  });

});
