import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableComponent } from './table/table.component';
import { TableInfinityScrollComponent } from './table-infinity-scroll/table-infinity-scroll.component';

const routes: Routes = [
  {
    path: 'home',
    component: TableComponent
  },
  {
    path: 'dashboard',
    component: TableInfinityScrollComponent
  },
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
