import { Component, OnInit, AfterViewChecked } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../service/data.service';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit, AfterViewChecked {

  numberOfRows = 10;
  currentPageNo = 1;
  startRowIndex = 0;
  totalNumberOfPages: number;
  dataObs$ = new Observable();
  dataList = [];
  displayDataList = [];

  constructor(
    private dataService: DataService,
    private apiService: ApiService
  ) {}

  ngOnInit() {

    this.dataObs$ = this.dataService.getData();
    this.dataService.getData().subscribe((data) => {
      this.dataList = data;
      this.calculateTotalPages(true);
    }, () => {
      console.log('Anonymous error!');
    });

  }

  ngAfterViewChecked() {
    this.floatFooter();
  }

  calculateTotalPages(reset: boolean) {
    this.totalNumberOfPages = Math.round(this.dataList.length / this.numberOfRows);

    this.totalNumberOfPages = (this.totalNumberOfPages === 0) ? 1 : this.totalNumberOfPages;

    // reset the counters
    if (reset) {
      this.currentPageNo = 1;
      this.startRowIndex = 0;
    }
    this.sliceDataList();
  }

  sliceDataList() {
    this.displayDataList = [].concat([], this.dataList.slice(this.startRowIndex, this.startRowIndex + this.numberOfRows));
  }

  previousPage() {
    if (this.currentPageNo > 1) {
      this.currentPageNo--;
      this.startRowIndex = (this.currentPageNo * this.numberOfRows) - 10;
      this.calculateTotalPages(false);
    }
  }

  nextPage() {
    if (this.currentPageNo < this.totalNumberOfPages) {
      this.startRowIndex = this.currentPageNo * this.numberOfRows + 1;

      this.currentPageNo++;
      this.calculateTotalPages(false);
    }
  }

  floatFooter() {
    const tableElem = document.querySelector('table');
    const elemBound = tableElem.getBoundingClientRect();

    const headerElem = document.querySelector('thead');
    const headerBoundElem = tableElem.getBoundingClientRect();

    const footerElem = document.querySelector('tfoot');
    if (elemBound.height > 400) {
      headerElem.style.top = headerBoundElem.top + 'px';
      headerElem.style.backgroundColor = '#878f99';
      headerElem.style.width = headerBoundElem.width + 'px';

      footerElem.style.position = 'absolute';
      footerElem.style.top = '582px';
      footerElem.style.backgroundColor = '#878f99';
      footerElem.style.width = '98%';
      footerElem.style.left = '7px';

    } else {
      footerElem.style.position = 'relative';
      headerElem.style.position = 'relative';
    }
  }

  submitRowStatus(row: any) {
    console.log(row);
    this.apiService.submitRowDetails(row).subscribe((response) => {
    // logic in case of success goes here
    console.log(response);
    },
      (err) => {
      // logic in case of failure goes here
      console.log(err);
    });
  }
}
