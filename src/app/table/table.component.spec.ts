import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableComponent } from './table.component';
import { FormsModule } from '@angular/Forms';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { DataService } from '../service/data.service';
import { ApiService } from '../service/api.service';

describe('TableComponent', () => {
  let component: TableComponent;
  let fixture: ComponentFixture<TableComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        FormsModule,
        HttpClientTestingModule
      ],
      declarations: [ TableComponent ],
      providers: [
        DataService,
        ApiService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should calculate totalpages', () => {
    const totalPages = 1;
    const mockData = [{
        name: 'test-name-1',
        id: 1
      }, {
        name: 'test-name-2',
        id: 2
      }];

    component.calculateTotalPages(true);
    expect(component.totalNumberOfPages).toBe(totalPages);
  });

  it('should slice the records per page size', () => {
    const itemLength = 2;
    const mockData = [{
        name: 'test-name-1',
        id: 1
      }, {
        name: 'test-name-2',
        id: 2
      }];

    component.sliceDataList();
    component.displayDataList = mockData;
    expect(component.displayDataList.length).toBe(2);
  });


});
