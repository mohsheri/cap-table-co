import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { DataService } from '../service/data.service';
import { ApiService } from '../service/api.service';

@Component({
  selector: 'app-table-infinity-scroll',
  templateUrl: './table-infinity-scroll.component.html',
  styleUrls: ['./table-infinity-scroll.component.scss']
})
export class TableInfinityScrollComponent implements OnInit {

  numberOfRows = 10;
  currentPageNo = 1;
  startRowIndex = 0;
  totalNumberOfPages: number;
  dataObs$ = new Observable();
  dataList = [];
  displayDataList = [];

  constructor(
    private dataService: DataService,
    private apiService: ApiService
  ) {}

  ngOnInit() {

    this.dataObs$ = this.dataService.getData();
    this.dataService.getData().subscribe((data) => {
      this.dataList = data;
      // this.calculateTotalPages(true);
    }, () => {
      console.log('Anonymous error!');
    });

  }

  submitRowStatus(row: any) {
    this.apiService.submitRowDetails(row).subscribe((response) => {
    // logic in case of success goes here
    console.log(response);
    },
      (err) => {
      // logic in case of failure goes here
      console.log(err);
    });
  }
}
