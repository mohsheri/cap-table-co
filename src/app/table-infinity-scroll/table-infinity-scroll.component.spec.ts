import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TableInfinityScrollComponent } from './table-infinity-scroll.component';
import { DataService } from '../service/data.service';
import { ApiService } from '../service/api.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ScrollingModule } from '@angular/cdk/scrolling';

describe('TableInfinityScrollComponent', () => {
  let component: TableInfinityScrollComponent;
  let fixture: ComponentFixture<TableInfinityScrollComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        ScrollingModule
      ],
      declarations: [
        TableInfinityScrollComponent
      ],
      providers: [
        DataService,
        ApiService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TableInfinityScrollComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
